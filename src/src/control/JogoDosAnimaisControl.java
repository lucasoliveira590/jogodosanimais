/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package src.control;

import src.model.AnimalNode;

/**
 *
 * @author Lucas
 */
public final class JogoDosAnimaisControl {
    private AnimalNode tree;
    private AnimalNode currentAnimalNode;
    public JogoDosAnimaisControl(){        
        tree = addBaseAnimals();
        currentAnimalNode = this.getTree();
    }
    
    //Animais padrões de inicialização do sistema
    private AnimalNode addBaseAnimals() {
        AnimalNode root = new AnimalNode("tubarão", "vive na água");
        AnimalNode shark = new AnimalNode("tubarão", "vive na água");
        AnimalNode monkey  = new AnimalNode("macaco", "não vive na água");
        root.setRightChild(shark);
        root.setLeftChild(monkey);
        return root;
    }
    
    // Folha anterior é copiada para o lado left (negativo)
    // Adiciona uma novo folha do lado right (positivo)
    // Nó anterior virou pai
    public void addNewAnimal(String newAnimalName, String newAnimalFeature) {
        AnimalNode newAnimal = new AnimalNode(newAnimalName, newAnimalFeature);
        AnimalNode stepAnimal = new AnimalNode(currentAnimalNode.getAnimalName(), currentAnimalNode.getAnimalFeature());
        currentAnimalNode.setLeftChild(stepAnimal);
        currentAnimalNode.setRightChild(newAnimal);
        currentAnimalNode = this.getTree();
    }
    

    /**
     * @return the tree
     */
    public AnimalNode getTree() {
        return tree;
    }

    /**
     * @param tree the tree to set
     */
    public void setTree(AnimalNode tree) {
        this.tree = tree;
    }

    /**
     * @return the currentAnimalNode
     */
    public AnimalNode getCurrentAnimalNode() {
        return currentAnimalNode;
    }

    /**
     * @param currentAnimalNode the currentAnimalNode to set
     */
    public void setCurrentAnimalNode(AnimalNode currentAnimalNode) {
        this.currentAnimalNode = currentAnimalNode;
    }

    public void stepRight() {
        this.currentAnimalNode = this.currentAnimalNode.getRightChild();
    }
    
    public void stepLeft() {
        this.currentAnimalNode = this.currentAnimalNode.getLeftChild();
    }
}
