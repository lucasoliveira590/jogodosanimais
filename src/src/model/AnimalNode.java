/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package src.model;

/**
 *
 * @author Lucas
 */
public class AnimalNode {
    private String animalName;
    private String animalFeature;
     
    private AnimalNode leftChild;
    private AnimalNode rightChild;
    
    public AnimalNode(String animalNome, String animalFeature){
        this.animalName = animalNome;
        this.animalFeature = animalFeature;
        this.leftChild = null;
        this.rightChild = null;
    }
    
    /**
     * @return the feature question for the node
     */
    public String getFeatureQuestion(){
        return "O animal que você pensou " + this.getAnimalFeature() + "?";
    }
    
    /**
     * @return the feature question for the node
     */
    public String getAnimalQuestion(){
        return "O animal que você pensou é " + this.getAnimalName() + "?";
    }

    /**
     * @return the animalName
     */
    public String getAnimalName() {
        return animalName;
    }

    /**
     * @param animalName the animalName to set
     */
    public void setAnimalName(String animalName) {
        this.animalName = animalName;
    }

    /**
     * @return the animalFeature
     */
    public String getAnimalFeature() {
        return animalFeature;
    }

    /**
     * @param animalFeature the animalFeature to set
     */
    public void setAnimalFeature(String animalFeature) {
        this.animalFeature = animalFeature;
    }

    /**
     * @return the hasSon
     */
    public boolean hasSon() {
        return (getLeftChild() != null) || (getRightChild() != null);
    }


    /**
     * @return the leftChild
     */
    public AnimalNode getLeftChild() {
        return leftChild;
    }

    /**
     * @param leftChild the leftChild to set
     */
    public void setLeftChild(AnimalNode leftChild) {
        this.leftChild = leftChild;
    }

    /**
     * @return the rightChild
     */
    public AnimalNode getRightChild() {
        return rightChild;
    }

    /**
     * @param rightChild the rightChild to set
     */
    public void setRightChild(AnimalNode rightChild) {
        this.rightChild = rightChild;
    }

}
